// MongoDB Operations
// For creating or inserting data into database

db.users.insert({
    firstName: "Ely",
    lastName: "Buendia",
    age: 50,
    contact: {
        phone: "12345",
        email: "ely@eraserheads.com"
    },
    courses: ["CSS", "JavaScript", "Python"],
    department: "none"
});


// for inserting many documents at the same time

db.users.insertMany([
    {
        firstName: "Chito",
        lastName: "Miranda",
        age: 43,
        contact: {
            phone: "5347786",
            email: "chito@parokya.com"
        },
        courses: ["Python", "React", "PHP"],
        department: "none"
    },
    {
        firstName: "Francis",
        lastName: "Magalona",
        age: 61,
        contact: {
            phone: "5347786",
            email: "francisM@email.com"
        },
        courses: ["React", "Laravel", "SASS"],
        department: "none"
    }
])
//=========================================//

// for querying all the data in database
// for finding documents

// FIND ALL

db.users.find();

// FIND [One]

db.users.find({ firstName: "Francis", age: 61 });


//=========================================//

// Delete

// Single Deletion
db.users.deleteOne({
    firstName: "Ely"
});

// Multiple Deletion
db.users.deleteMany(
    {
        department: "DOH"
    }

);
//=========================================//

// Update

// Single Update
db.users.updateOne(
    {
        firstName: "Gloc-8"
    },
    {
        $set: {
            department: "DOH"
        }
    }
)

// Multiple Updates
db.users.updateMany(
    {
        department: "none"
    },
    {
        $set: {
            department: "HR"
        }
    }
);